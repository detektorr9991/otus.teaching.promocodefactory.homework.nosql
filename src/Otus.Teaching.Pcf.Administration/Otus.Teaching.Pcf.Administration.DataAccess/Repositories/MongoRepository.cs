﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Settings;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T>: IRepository<T> where T: BaseEntity
    {
        private readonly IMongoCollection<T> _collection;
        
        public MongoRepository(IMongoSettings settings)
        {
            var database = new MongoClient(settings.ConnectionString).GetDatabase(settings.DbName);
            var name = GetCollectionName(typeof(T));
            
            _collection = database.GetCollection<T>(name);
        }
        
        public static  string GetCollectionName(Type documentType)
        {
            return ((CollectionAttribute)documentType
                .GetCustomAttributes(typeof(CollectionAttribute), true)
                .FirstOrDefault())?.Collection;
        }
        
        public async  Task<IEnumerable<T>> GetAllAsync()
        {
            return await _collection.AsQueryable().ToListAsync();
            
        }
        
        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await _collection.FindOneAndReplaceAsync(filter => filter.Id == entity.Id, entity);
        }
        
        public async Task DeleteAsync(T entity)
        {
            await _collection.DeleteOneAsync(filter => filter.Id == entity.Id);
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _collection.Find(filter => filter.Id == id).FirstAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find(predicate).FirstAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _collection.Find(filter => ids.Contains(filter.Id)).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find(predicate).ToListAsync();
        }
    }
}