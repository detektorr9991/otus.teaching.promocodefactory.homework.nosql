﻿
using System;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    public class CollectionAttribute : Attribute
    {
        public string Collection { get; }

        public CollectionAttribute(string collection)
        {
            Collection = collection;
        }
    }
}